package id.ac.ugm.customerservice.integration;

import static org.junit.Assert.assertEquals;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import id.ac.ugm.customerservice.entity.Customer;
import id.ac.ugm.customerservice.repo.CustomerRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CustomerRepositoryIntegrationTests {

  private final String CUSTOMER_NAME = "Alman";

  @Autowired
  private TestEntityManager entityManager;

  @Autowired
  private CustomerRepository customerRepository;

  @Test
  public void whenFindByUsername_thenReturnCustomer() {
    // given
    Customer customer = new Customer(CUSTOMER_NAME);
    entityManager.persist(customer);
    entityManager.flush();

    // when
    Optional<Customer> found = customerRepository.findByUsername(CUSTOMER_NAME);

    // then
    assertEquals(found.get().getUsername(), CUSTOMER_NAME);
  }
}